#pragma once
#include <windows.h>	
#include <gl/gl.h>		
#include <gl/glu.h>		

class Bullet
{
public:

	Bullet(const float* vertices, const float* colors) : BTVertices(vertices), BTColors(colors)
	{
		bTXpos = 100.0f;
		bTYpos = 100.0f;
		bRotate = 0.0f;
		bRotationRads = 0.0f; 
		bSpeed = 0.0f; 
	}
	
	// Getters & Setters
	void setbTXpos(float bBTXpos)
	{
		bTXpos = bBTXpos;
	}
	float getbTXpos(void)
	{
		return bTXpos;
	}
	void setbTYpos(float bBTYpos)
	{
		bTYpos = bBTYpos;
	}
	float getbTYpos(void)
	{
		return bTYpos;
	}
	void setbRotate(float bBRotate)
	{
		bRotate = bBRotate;
	}
	float getbRotate(void)
	{
		return bRotate;
	}
	void setbRotationRads(float bBRotationRads)
	{
		bRotationRads = bBRotationRads;
	}
	float getbRotationRads(void)
	{
		return bRotationRads;
	}
	void setbSpeed(float bBspeed)
	{
		bSpeed = bBspeed;
	}
	float getbSpeed(void)
	{
		return bSpeed;
	}
	
	// Functions
	void update(float scale, float xWidth, float yWidth)
	{
		bRotationRads = bRotate * TO_RADS;
		bTXpos = bTXpos + (scale * bSpeed * cos(bRotationRads));
		bTYpos = bTYpos + (scale * bSpeed * sin(bRotationRads));

		if (bTXpos > xWidth || bTXpos < -xWidth || bTYpos > yWidth || bTYpos < -yWidth)
		{
			reset();
		}

	}
	void draw(void)
	{
		glVertexPointer(3, GL_FLOAT, 0, BTVertices);
		glColorPointer(4, GL_FLOAT, 0, BTColors);
		glLoadIdentity();
		glTranslatef(bTXpos, bTYpos, -5.0);
		glRotatef(bRotate, 0.0, 0.0, 1.0);
		glScalef(0.2f, 0.2f, 0.2f);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
	void reset(void)
	{
		bTXpos = 100.0f;
		bTYpos = 100.0f;
		bSpeed = 0.0f;
		setInTransit(false);
	}
	bool getInTransit(void)
	{
		return inTransit;
	}
	void setInTransit(bool transit)
	{
		inTransit = transit;
	}

private:
	const float TO_RADS = 0.01745f;
	float bTXpos;
	float bTYpos;
	float bRotate;
	float bRotationRads;
	float bSpeed; 
	bool inTransit = false;

	const float* BTVertices;
	const float* BTColors;
};