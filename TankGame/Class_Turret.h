#pragma once
#include <windows.h>	
#include <gl/gl.h>		
#include <gl/glu.h>	

const float BTVertices[18] = {

	-0.1f, -0.1f, 0.0f,

	0.1f, -0.1f, 0.0f,   // FIRST TRIANGLE

	-0.1f, 0.1f, 0.0f,

	// @@@@@@@@@@@@@@@@@@@@@@@@@@

	0.1f, 0.1f, 0.0f,

	-0.1f, 0.1f, 0.0f,	// SECOND TRIANGLE

	0.1f, -0.1f, 0.0f,
};

const float BTColors[24] = {

	0.0f, 0.0f, 0.0f, 1.0f,

	0.0f, 0.0f, 0.0f, 1.0f,		// FIRST TRIANGLE

	0.0f, 0.0f, 0.0f, 1.0f,

	// @@@@@@@@@@@@@@@@@@@@@@@

	0.0f, 0.0f, 0.0f, 1.0f,

	0.0f, 0.0f, 0.0f, 1.0f,		// SECOND TRIANGLE

	0.0f, 0.0f, 0.0f, 1.0f
};

class Turret
{
public:

	Turret() : bullet(BTVertices, BTColors)
	{
		tuXpos = 0.0f;
		tuYpos = 0.0f;
		tuRotate = 0.0f;
		tuRotationRads = 0.0f;
	}

	//Getters
	float gettuXpos(void)
	{
		return tuXpos;
	}
	float gettuYpos(void)
	{
		return tuYpos;
	}
	void settuRotationRads(float TtuRotationRads)
	{
		tuRotationRads = TtuRotationRads;
	}
	float gettuRotationRads(void)
	{
		return tuRotationRads;
	}
	Bullet& getBullet(void)
	{
		return bullet;
	}

	
	//Functions
	void update(float scale, float TtuXpos, float TtuYpos, float xWidth, float yWidth)
	{
		tuXpos = TtuXpos;
		tuYpos = TtuYpos;
		bullet.update(scale, xWidth, yWidth);
	}
	void reset(void)
	{
		tuRotate = 0.0f;
		bullet.reset();
	}
	void left(float scale)
	{
		settuRotate(tuRotate + ROTATION_SPEED * scale);
	}
	void right(float scale)
	{
		settuRotate(tuRotate - ROTATION_SPEED * scale);
	}
	void fire(void)
	{
		if (!bullet.getInTransit())
		{
			bullet.setbTXpos(tuXpos);
			bullet.setbTYpos(tuYpos);
			bullet.setbRotate(tuRotate);
			bullet.setbSpeed(BULLET_SPEED);
			bullet.setInTransit(true);
		}
	}
	void draw(void)
	{
		glVertexPointer(3, GL_FLOAT, 0, BTVertices);
		glColorPointer(4, GL_FLOAT, 0, BTColors);
		glLoadIdentity();
		glTranslatef(tuXpos, tuYpos, -5.0);
		glRotatef(tuRotate, 0.0, 0.0, 1.0);
		glScalef(0.6f, 0.4f, 0.6f);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		bullet.draw();

	}

private:
	const float ROTATION_SPEED = 1.0f;
	const float BULLET_SPEED = 0.005f;
	float tuXpos;
	float tuYpos;
	float tuRotate;
	float tuRotationRads;

	Bullet bullet;
	// Private Getters & Setters
	void settuRotate(float TtuRotate)
	{
		tuRotate = TtuRotate;
		bullet.setbRotate(tuRotate);
	}
	float gettuRotate(void)
	{
		return tuRotate;
	}
};