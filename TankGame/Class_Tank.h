#pragma once
#include <windows.h>	
#include <gl/gl.h>		
#include <gl/glu.h>	
#include "Class_Bullets.h"
#include "Class_Turret.h"
#include "Class_Random.h"

const float tankVertices[18] = {

	-0.2f, -0.1f, 0.0f,

	0.2f, -0.1f, 0.0f,  // FIRST TRIANGLE

	-0.2f, 0.1f, 0.0f,

	// @@@@@@@@@@@@@@@@@@@@@@@

	0.2f, 0.1f, 0.0f,

	-0.2f, 0.1f, 0.0f,	// SECOND TRIANGLE

	0.2f, -0.1f, 0.0f,
};

class Tank
{
public:
	
	

	const float NORM_MAX_SPEED = 0.015f;
	
	Tank()
	{
		TXpos = 0.0f;
		TYpos = 0.0f;
		TRotate = 0.0f;
		TSpeed = 0.0f;
		rotationRads = 0.0f;
		maxSpeed = NORM_MAX_SPEED;
		playerTimer = 4000.0f;
		turnLeft = false;
		turnRight = false;
		turnUp = false;
		turnDown = false;
		fireWeapon = false;
		reload = true; 
		turretRight = false;
		turretLeft = false;
	}
	~Tank()
	{

	}

	//Getters & Setters
	void setTXpos(float TAXpos)
	{
		TXpos = TAXpos;
	}
	float getTXpos(void)
	{
		return TXpos;
	}
	void setTYpos(float TAYpos)
	{
		TYpos = TAYpos;
	}
	float getTYpos(void)
	{
		return TYpos;
	}
	void setTRotate(float TARotate)
	{
		TRotate = TARotate;
	}
	float getTRotate(void)
	{
		return TRotate;
	}
	void setrotationRads(float TRR)
	{
		rotationRads = TRR;
	}
	float getrotationRads(void)
	{
		return rotationRads;
	}
	void setTSpeed(float TASpeed)
	{
		TSpeed = TASpeed;
	}
	float getTSpeed(void)
	{
		return TSpeed;
	}
	void setmaxSpeed(float TAmaxSpeed)
	{
		maxSpeed = TAmaxSpeed;
	}
	float getmaxSpeed(void)
	{
		return maxSpeed;
	}
	bool getfireWeapon(void)
	{
		return fireWeapon;
	}
	void setfireWeapon(bool TAfireWeapon)
	{
		fireWeapon = TAfireWeapon;
	}
	bool getturnLeft(void)
	{
		return turnLeft;
	}
	void setturnLeft(bool TAturnLeft)
	{
		turnLeft = TAturnLeft;
	}
	bool getturnRight(void)
	{
		return turnRight;
	}
	void setturnRight(bool TAturnRight)
	{
		turnRight = TAturnRight;
	}
	bool getturnUp(void)
	{
		return turnUp;
	}
	void setturnUp(bool TAturnUp)
	{
		turnUp = TAturnUp;
	}
	bool getturnDown(void)
	{
		return turnDown;
	}
	void setturnDown(bool TAturnDown)
	{
		turnDown = TAturnDown;
	}
	void setreload(bool TAreload)
	{
		reload = TAreload;
	}
	bool getreload(void)
	{
		return reload;
	}
	void setplayertimer(float Tplayertimer)
	{
		playerTimer = Tplayertimer;
	}
	float getplayertimer(void)
	{
		return playerTimer;
	}
	void setturretLeft(bool TturretLeft)
	{
		turretLeft = TturretLeft;
	}
	bool getturretLeft(void)
	{
		return turretLeft;
	}
	void setturretRight(bool TturretRight)
	{
		turretRight = TturretRight;
	}
	bool getturretRight(void)
	{
		return turretRight;
	}
	
	//Functions for tank
	void accelerate(void)
	{
		if (TSpeed < maxSpeed) {
			TSpeed = TSpeed + SPEED_INC;
		}
	}
	void deccelerate(void)
	{
		if (TSpeed >= -maxSpeed) {
			TSpeed = TSpeed - SPEED_DEC;
		}
	}
	void left(float scale)
	{
		TRotate = TRotate + ROTATION_SPEED * scale;
	}
	void right(float scale)
	{
		TRotate = TRotate - ROTATION_SPEED * scale;
	}
	void stop(void)
	{
		TSpeed = 0;
	}
	void update(float scale, float xWidth, float yWidth)
	{
		rotationRads = TRotate * TO_RADS;
		TXpos = TXpos + (scale * TSpeed * cos(rotationRads));
		TYpos = TYpos + (scale * TSpeed * sin(rotationRads));
		turret.update(scale, TXpos, TYpos, xWidth, yWidth);
	}
	void reset(void)
	{
		setTYpos(0.0f);
		setTXpos(0.0f);
		setTRotate(0.0f);
		setTSpeed(0.0f);
		turret.reset();
	}
	bool detectCollision(Bullet extBullet, float distThreshold)
	{
		float xDiff = (extBullet.getbTXpos() - TXpos);	// this is used to determine the distance between the two players
		float yDiff = (extBullet.getbTYpos() - TYpos);
		float xDiffSquared = pow(xDiff, 2);
		float yDiffSquared = pow(yDiff, 2);
		float total = (xDiffSquared + yDiffSquared);

		float distance = sqrt(total);

		if (distance <= distThreshold) // if the distane is less than or equal to declared distance...	
		{
			return true;   // return a true variable, or...
		}
		else
		{
			return false; // return a false variable...
		}
	}
	bool detectCollision(float rXpos, float rYpos, float distThreshold)
	{
		float xDiff = (rXpos - TXpos);	// this is used to determine the distance between the two players
		float yDiff = (rYpos - TYpos);
		float xDiffSquared = pow(xDiff, 2);
		float yDiffSquared = pow(yDiff, 2);
		float total = (xDiffSquared + yDiffSquared);

		float distance = sqrt(total);

		if (distance <= distThreshold) // if the distane is less than or equal to declared distance...	
		{
			return true;   // return a true variable, or...
		}
		else
		{
			return false; // return a false variable...
		}
	}
	void draw(void)
	{
		glVertexPointer(3, GL_FLOAT, 0, tankVertices);  // (3) dimensional
		glColorPointer(4, GL_FLOAT, 0, tankColors);
		glLoadIdentity();    // resets the pen back to 0
		glTranslatef(TXpos, TYpos, -5.0);		// changes the positions of the object by said amount
		glRotatef(TRotate, 0.0, 0.0, 1.0);								// changes the rotation of the object by said amount 
		glScalef(0.8f, 0.8f, 0.8f);
		glDrawArrays(GL_TRIANGLES, 0, 6);														// draws the amount of vertices given 

		turret.draw();
	}
	void setColor(const float* color)
	{
		tankColors = color;
	}
	Turret& getTurret()
	{
		return turret;
	}
	Random& getRandom()
	{
		return random;
	}

private:
	const float ROTATION_SPEED = 1.0f;
	const float SPEED_INC = 0.00005f;
	const float SPEED_DEC = 0.00005f;
	const float TO_RADS = 0.01745f;
	float TXpos;
	float TYpos;
	float TRotate;
	float rotationRads;
	float TSpeed;
	float maxSpeed; 
	float playerTimer;
	bool fireWeapon;
	bool turnLeft;
	bool turnRight;
	bool turnUp;
	bool turnDown;
	bool reload;
	bool turretRight;
	bool turretLeft;

	Turret turret;
	Random random;
	const float* tankColors;
};