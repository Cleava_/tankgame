#pragma once
#include <windows.h>	
#include <gl/gl.h>		
#include <gl/glu.h>		

class Random
{
public:

	Random()
	{
		rTXpos = 100.0f;
		rTYpos = 0.0f;
		rRotate = 0.0f;
		wMinute = 2.0f;
		mMinute = 2.0f;
	}

	//Getteres&Setters
	void setrTXpos(float RrTXpos)
	{
		rTXpos = RrTXpos;
	}
	float getrTXpos(void)
	{
		return rTXpos;
	}
	void setrTYpos(float RrTYPos)
	{
		rTYpos = RrTYPos;
	}
	float getrTYpos(void)
	{
		return rTYpos;
	}
	void setrRotate(float RrRotate)
	{
		rRotate = RrRotate;
	}
	float getrRotate(void)
	{
		return rRotate;
	}


	//Functions

private:
	float rTXpos;
	float rTYpos;
	float rRotate;
	float wMinute;
	float mMinute;

};