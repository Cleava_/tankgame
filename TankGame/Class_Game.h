#pragma once
#include "Class_Tank.h"
#include "Class_Bullets.h"
#include "Class_Turret.h"
#include "Class_Random.h"
#include <windows.h>	
#include <gl/gl.h>		
#include <gl/glu.h>		

const float tankVertexColorsTankOne[24] = {

	1.0f, 0.0f, 0.0f, 1.0f,

	1.0f, 0.0f, 0.0f, 1.0f,		// FIRST TRIANGLE

	1.0f, 0.0f, 0.0f, 1.0f,

	// @@@@@@@@@@@@@@@@@@@@@@@

	1.0f, 0.0f, 0.0f, 1.0f,

	1.0f, 0.0f, 0.0f, 1.0f,		// SECOND TRIANGLE

	1.0f, 0.0f, 0.0f, 1.0f
};


const float tankVertexColorsTankTwo[24] = {

	0.0f, 0.0f, 1.0f, 1.0f,

	0.0f, 0.0f, 1.0f, 1.0f,		// FIRST TRIANGLE

	0.0f, 0.0f, 1.0f, 1.0f,

	// @@@@@@@@@@@@@@@@@@@@@@@

	0.0f, 0.0f, 1.0f, 1.0f,

	0.0f, 0.0f, 1.0f, 1.0f,		// SECOND TRIANGLE

	0.0f, 0.0f, 1.0f, 1.0f
};

class Game
{
public:
	LARGE_INTEGER frameStartTime, frameEndTime, frameDelta;
	LARGE_INTEGER frequency;
	float scaleFactor;
	float powerUpTimer;
	bool powerUpSpawned;
	
	
	
	
	Tank * playerPointer;
	Random * randomPointer; 

	Game()
	{
		frameStartTime.QuadPart = 0;
		frameEndTime.QuadPart = 0;
		frameDelta.QuadPart = 0;
		scaleFactor = 0.0f;
		powerUpTimer = 4000.0f;
		powerUpSpawned = true;

		playerPointer = new Tank[2];
		playerPointer[0].setColor(tankVertexColorsTankOne);
		playerPointer[1].setColor(tankVertexColorsTankTwo);
		randomPointer = new Random[2];
	}
	~Game()
	{
		delete[] playerPointer;
		playerPointer = NULL;

		delete[] randomPointer;
		randomPointer = NULL;
	}

};