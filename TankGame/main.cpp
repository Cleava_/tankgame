

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// OpenGL without using GLUT - 2013 by Neil Dansey, Tim Dykes and Ian Cant, and using excerpts from here:
// http://bobobobo.wordpress.com/2008/02/11/opengl-in-a-proper-windows-app-no-glut/
// Feel free to adapt this for what you need, but please leave these comments in.

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


#pragma once

#include <windows.h>	// need this file if you want to create windows etc
#include <gl/gl.h>		// need this file to do graphics with opengl
#include <gl/glu.h>		// need this file to set up a perspective projection easily
#include <math.h>       // need this file to use sqrt() and pow()
#include <stdlib.h>
#include <time.h>
#define idealFrameRate 80
#define idealDelta (100000.0f / idealFrameRate)
#include "Class_Tank.h"
#include "Class_Game.h"
#include "Class_Bullets.h"
#include "Class_Turret.h"
#include "Class_Random.h"

// include the opengl and glu libraries
#pragma comment(lib, "opengl32.lib")	
#pragma comment(lib, "glu32.lib")

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

										// determines the vertices and colours of the tanks

float randomVertices[18] = {

	-0.1f, -0.1f, 0.0f,

	0.1f, -0.1f, 0.0f,   // FIRST TRIANGLE

	-0.1f, 0.1f, 0.0f,

	// @@@@@@@@@@@@@@@@@@@@@@@@@@

	0.1f, 0.1f, 0.0f,

	-0.1f, 0.1f, 0.0f,	// SECOND TRIANGLE

	0.1f, -0.1f, 0.0f,
};

float randomVertexColours[24] = {

	0.0f, 1.0f, 0.0f, 1.0f,

	0.0f, 1.0f, 0.0f, 1.0f,		// FIRST TRIANGLE

	0.0f, 1.0f, 0.0f, 1.0f,
	
	// @@@@@@@@@@@@@@@@@@@@@@@

	0.0f, 1.0f, 0.0f, 1.0f,

	0.0f, 1.0f, 0.0f, 1.0f,		// SECOND TRIANGLE

	0.0f, 1.0f, 0.0f, 1.0f
};

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


// function prototypes:
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow);
void draw(Game *TGAME);
void update(Game *UPTGAME);
void spawnPowerUps(Game * RTGame);
void refreshPowerUps(Game * RPGame);

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

const enum KEY_MAP {PL_1_ACC		= 'W',		// this assigns the keys to the correct definition
					PL_1_DEC		= 'S',
					PL_1_LEFT		= 'A',
					PL_1_RIGHT		= 'D', 
					PL_1_FIRE		= 'F',
					PL_1_T_RIGHT	= 'E', 
					PL_1_T_LEFT		= 'Q', 
					PL_2_ACC		= 'I', 
					PL_2_DEC		= 'K', 
					PL_2_LEFT		= 'J', 
					PL_2_RIGHT		= 'L', 
					PL_2_FIRE		= 'H', 
					PL_2_T_RIGHT	= 'O', 
					PL_2_T_LEFT		= 'U', 
					APP_QUIT		= 27, // ESC
};

// In a C++ Windows app, the starting point is WinMain() rather than _tmain() or main().
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow)
{
	// some basic numbers to hold the position and size of the window
	int windowWidth = 800;
	int windowHeight = 600;
	int windowTopLeftX = 50;
	int windowTopLeftY = 50;

	// some other variables we need for our game...
	MSG msg;								// this will be used to store messages from the operating system
	bool keepPlaying = true;				// whether or not we want to keep playing
	




	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// this section contains all the window initialisation code, 
	// and should probably be collapsed for the time being to avoid confusion	
#pragma region  <-- click the plus/minus sign to collapse/expand!

	// this bit creates a window class, basically a template for the window we will make later, so we can do more windows the same.
	WNDCLASS myWindowClass;
	myWindowClass.cbClsExtra = 0;											// no idea
	myWindowClass.cbWndExtra = 0;											// no idea
	myWindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	// background fill black
	myWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);					// arrow cursor       
	myWindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);				// type of icon to use (default app icon)
	myWindowClass.hInstance = hInstance;									// window instance name (given by the OS when the window is created)   
	myWindowClass.lpfnWndProc = WndProc;									// window callback function - this is our function below that is called whenever something happens
	myWindowClass.lpszClassName = TEXT("my window class name");				// our new window class name
	myWindowClass.lpszMenuName = 0;											// window menu name (0 = default menu?) 
	myWindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;				// redraw if the window is resized horizontally or vertically, allow different context for each window instance

	// Register that class with the Windows OS..
	RegisterClass(&myWindowClass);

	// create a rect structure to hold the dimensions of our window
	RECT rect;
	SetRect(&rect, windowTopLeftX,				// the top-left corner x-coordinate
		windowTopLeftY,				// the top-left corner y-coordinate
		windowTopLeftX + windowWidth,		// far right
		windowTopLeftY + windowHeight);	// far left

	// adjust the window, no idea why.
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, false);

	// call CreateWindow to create the window
	HWND myWindow = CreateWindow(TEXT("my window class name"),		// window class to use - in this case the one we created a minute ago
		TEXT("CT4TOGA Coursework Template"),		// window title
		WS_OVERLAPPEDWINDOW,						// ??
		windowTopLeftX, windowTopLeftY,			// x, y
		windowWidth, windowHeight,				// width and height
		NULL, NULL,								// ??
		hInstance, NULL);							// ??


	// check to see that the window created okay
	if (myWindow == NULL)
	{
		FatalAppExit(NULL, TEXT("CreateWindow() failed!")); // ch15
	}

	// if so, show it
	ShowWindow(myWindow, iCmdShow);


	// get a device context from the window
	HDC myDeviceContext = GetDC(myWindow);


	// we create a pixel format descriptor, to describe our desired pixel format. 
	// we set all of the fields to 0 before we do anything else
	// this is because PIXELFORMATDESCRIPTOR has loads of fields that we won't use
	PIXELFORMATDESCRIPTOR myPfd = { 0 };


	// now set only the fields of the pfd we care about:
	myPfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);		// size of the pfd in memory
	myPfd.nVersion = 1;									// always 1

	myPfd.dwFlags = PFD_SUPPORT_OPENGL |				// OpenGL support - not DirectDraw
		PFD_DOUBLEBUFFER |				// double buffering support
		PFD_DRAW_TO_WINDOW;					// draw to the app window, not to a bitmap image

	myPfd.iPixelType = PFD_TYPE_RGBA;					// red, green, blue, alpha for each pixel
	myPfd.cColorBits = 24;								// 24 bit == 8 bits for red, 8 for green, 8 for blue.
	// This count of color bits EXCLUDES alpha.

	myPfd.cDepthBits = 32;								// 32 bits to measure pixel depth.


	// now we need to choose the closest pixel format to the one we wanted...	
	int chosenPixelFormat = ChoosePixelFormat(myDeviceContext, &myPfd);

	// if windows didnt have a suitable format, 0 would have been returned...
	if (chosenPixelFormat == 0)
	{
		FatalAppExit(NULL, TEXT("ChoosePixelFormat() failed!"));
	}

	// if we get this far it means we've got a valid pixel format
	// so now we need to set the device context up with that format...
	int result = SetPixelFormat(myDeviceContext, chosenPixelFormat, &myPfd);

	// if it failed...
	if (result == NULL)
	{
		FatalAppExit(NULL, TEXT("SetPixelFormat() failed!"));
	}

	// we now need to set up a render context (for opengl) that is compatible with the device context (from windows)...
	HGLRC myRenderContext = wglCreateContext(myDeviceContext);

	// then we connect the two together
	wglMakeCurrent(myDeviceContext, myRenderContext);



	// opengl display setup
	glMatrixMode(GL_PROJECTION);	// select the projection matrix, i.e. the one that controls the "camera"
	glLoadIdentity();				// reset it
	gluPerspective(45.0, (float)windowWidth / (float)windowHeight, 1, 1000);	// set up fov, and near / far clipping planes
	glViewport(0, 0, windowWidth, windowHeight);							// make the viewport cover the whole window
	glClearColor(0.5, 0, 0, 1.0);											// set the colour used for clearing the screen

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#pragma endregion
	
	Game * GamePoint = new Game;
	
	GamePoint->playerPointer[0].setTXpos(-0.5f); // spawns player one
	GamePoint->playerPointer[1].setTXpos(0.5f);  // spawns player two 

	// main game loop starts here!
	QueryPerformanceFrequency(&GamePoint->frequency);

	// keep doing this as long as the player hasnt exited the app: 
	while (keepPlaying == true)
	{
		QueryPerformanceCounter(&GamePoint->frameStartTime);

		// we need to listen out for OS messages.
		// if there is a windows message to process...
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			KEY_MAP inputParam = static_cast<KEY_MAP>(msg.wParam);
			bool keyDown = false;
			if (msg.message == WM_KEYDOWN) {
				keyDown = true;
			}

			if (msg.message == WM_QUIT) {
				inputParam = APP_QUIT;
			}

			if (msg.message == WM_KEYUP || msg.message == WM_KEYDOWN || msg.message == WM_QUIT) {
				

				switch (inputParam) {
				case APP_QUIT:
					keepPlaying = false;
					break;
				case PL_1_ACC:
					if (keyDown) {
						GamePoint->playerPointer[0].accelerate();
					}
					else
					{
						GamePoint->playerPointer[0].stop();
					}
					
					break;
				case PL_1_DEC:
					if (keyDown) {
						GamePoint->playerPointer[0].deccelerate();
					}
					else
					{
						GamePoint->playerPointer[0].stop();
					}
					break;
				case PL_1_FIRE:
					GamePoint->playerPointer[0].getTurret().fire();
					break;
				case PL_1_LEFT:
					GamePoint->playerPointer[0].left(GamePoint->scaleFactor);
					break;
				case PL_1_RIGHT:
					GamePoint->playerPointer[0].right(GamePoint->scaleFactor);
					break;
				case PL_1_T_LEFT:
					GamePoint->playerPointer[0].getTurret().left(GamePoint->scaleFactor);
					break;
				case PL_1_T_RIGHT:
					GamePoint->playerPointer[0].getTurret().right(GamePoint->scaleFactor);
					break;
				case PL_2_ACC:
					if (keyDown) {
						GamePoint->playerPointer[1].accelerate();
					}
					else
					{
						GamePoint->playerPointer[1].stop();
					}

					break;
				case PL_2_DEC:
					if (keyDown) {
						GamePoint->playerPointer[1].deccelerate();
					}
					else
					{
						GamePoint->playerPointer[1].stop();
					}
					break;
				case PL_2_FIRE:
					GamePoint->playerPointer[1].getTurret().fire();
					break;
				case PL_2_LEFT:
					GamePoint->playerPointer[1].left(GamePoint->scaleFactor);
					break;
				case PL_2_RIGHT:
					GamePoint->playerPointer[1].right(GamePoint->scaleFactor);
					break;
				case PL_2_T_LEFT:
					GamePoint->playerPointer[1].getTurret().left(GamePoint->scaleFactor);
					break;
				case PL_2_T_RIGHT:
					GamePoint->playerPointer[1].getTurret().right(GamePoint->scaleFactor);
					break;
				default:

					break;
				}
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// clear screen
		// turn on the ability to use vertex arrays...
		glEnableClientState(GL_VERTEX_ARRAY);
		// select which OpenGL stack to use...

		update(GamePoint);										// determines whether the two objects have collided or not	
		draw(GamePoint);										// draws forward the shapes and locates the positions of them
		SwapBuffers(myDeviceContext);							// update graphics

		QueryPerformanceCounter(&GamePoint->frameEndTime);
		GamePoint->frameDelta.QuadPart = GamePoint->frameEndTime.QuadPart - GamePoint->frameStartTime.QuadPart;
		GamePoint->frameDelta.QuadPart *= 1000000;
		GamePoint->frameDelta.QuadPart /= GamePoint->frequency.QuadPart;										


		GamePoint->scaleFactor = (float)GamePoint->frameDelta.QuadPart / idealDelta;	// This works out the correct scale factor, to run at the same speed on all machines, using QPC and QPF.

	}


	// the next bit will therefore happen when the player quits the app,
	// because they are trapped in the previous section as long as (keepPlaying == true).

	// UNmake our rendering context (make it 'uncurrent')
	wglMakeCurrent(NULL, NULL);

	// delete the rendering context, we no longer need it.
	wglDeleteContext(myRenderContext);

	// release our window's DC from the window
	ReleaseDC(myWindow, myDeviceContext);


	// end the program
	return msg.wParam;
}

void draw(Game *TGAME)
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glMatrixMode(GL_MODELVIEW);
	
	// draw player 1...
	for (int i = 0; i <= 1; i++)
	{
		TGAME->playerPointer[i].draw();		// This runs a draw function inside the Tank Class, which activates turret draw and so on...
	}

	for (int i = 0; i <= 1; i++)
	{
		glVertexPointer(3, GL_FLOAT, 0, randomVertices);
		glColorPointer(4, GL_FLOAT, 0, randomVertexColours);
		glLoadIdentity();
		glTranslatef(TGAME->randomPointer[i].getrTXpos(), TGAME->randomPointer[i].getrTYpos(), -5.0);
		glRotatef(TGAME->randomPointer[i].getrRotate(), 0.0, 0.0, 1.0);
		glScalef(0.6f, 0.4f, 0.6f);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		
	}
}


float randomX()
{
	int r = rand(); // r = 0-32767
	r %= 58; // r = 0-57
	float f = (float)r;
	f -= 29; // f = -29 to +28
	f /= 19; // -2.9 to + 2.8

	return f;	// Creates a random number for the X parameter. 
}

float randomY()
{
	int rr = rand(); // r = 0-32767
	rr %= 42; // r = 0-44
	float ff = (float)rr;
	ff -= 21; // f = -21 to +20
	ff /= 10; // f = -2.1 to 2.0

	return ff; // Creates a random number for the Y paramter. 
}


void update(Game *UPTGAME)

{
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// Tank Controls
	for (int i = 0; i <= 1; i++)
	{
		UPTGAME->playerPointer[i].update(UPTGAME->scaleFactor, 3.0f, 2.20f);	// This updates everything on the screen. 
	}
	// DETECT COLLISION
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	bool TankOneBulletDetection = UPTGAME->playerPointer[0].detectCollision(UPTGAME->playerPointer[1].getTurret().getBullet(), 0.1f);
	bool TankTwoBulletDetection = UPTGAME->playerPointer[1].detectCollision(UPTGAME->playerPointer[0].getTurret().getBullet(), 0.1f);

	if ((TankOneBulletDetection == true) || (TankTwoBulletDetection == true))
	{
		for (int i = 0; i <= 1; i++)
		{
			UPTGAME->playerPointer[i].reset();
		}
		UPTGAME->playerPointer[0].setTXpos(-0.5f);
		UPTGAME->playerPointer[1].setTXpos(0.5f);
	}

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// POWERUP SPAWN

		UPTGAME->powerUpTimer -= 1.0f *UPTGAME->scaleFactor;
			// in your player class make a variable for max speed and use this when speeding up and slowing down // DONE
		if (UPTGAME->powerUpTimer <= 0.0f)							
		{
			UPTGAME->powerUpTimer = 4000.0f;
			UPTGAME->randomPointer[0].setrTXpos(randomX());
			UPTGAME->randomPointer[0].setrTYpos(randomY());
		}															// every frame, inside the update function
																					// take a small amount off the game timer
																					// if this is now <= 0, do the following:
																					// make the timer back to 500// re randomise the coordinates
		for (int i = 0; i <= 1; i++)
		{
			UPTGAME->playerPointer[i].getplayertimer() - 1.0f *UPTGAME->scaleFactor;

			if (UPTGAME->playerPointer[i].getplayertimer() <= 0.0f)
			{
				UPTGAME->playerPointer[i].setmaxSpeed(UPTGAME->playerPointer[i].NORM_MAX_SPEED);
			}
		}
																	// for each player do the following
																			// take small amount off the players timer (1 * scalefactor)
																			// if their timer is now <= 0 set their maxspeed to be 0.2f
		for (int i = 0; i <= 1; i++)
		{
			bool powCollision = UPTGAME->playerPointer[i].detectCollision(UPTGAME->randomPointer[i].getrTXpos(), UPTGAME->randomPointer[i].getrTYpos(), 0.1f);
			if (powCollision == true)
			{
				UPTGAME->randomPointer[0].setrTXpos(100.0f);
				UPTGAME->randomPointer[0].setrTYpos(100.0f);
				UPTGAME->playerPointer[0].setplayertimer(4000.0f);
				UPTGAME->powerUpTimer = 4000.0f;
				UPTGAME->playerPointer[0].setmaxSpeed(0.030f);
			}
		}
		


			// if a player has hit the power up
				// move the power up off screen(100 100)
				// set the players timer to be 300 or whatever (tank class)
				// set powerup's timer to be 500
				// set the players max speed to be the higher value (0.4f)


	// SCREEN WRAPPING
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	for (int i = 0; i <= 1; i++)
	{
		if (UPTGAME->playerPointer[i].getTXpos() > 3.0f)
		{
			UPTGAME->playerPointer[i].setTXpos(-3.0f); // wraps tank 1 on the right side on the screen
		}
		else if (UPTGAME->playerPointer[i].getTXpos() < -3.0f)
		{
			UPTGAME->playerPointer[i].setTXpos(3.0f);// wraps tank 1 on the left side on the screen
		}
		if (UPTGAME->playerPointer[i].getTYpos() > 2.20f)
		{
			UPTGAME->playerPointer[i].setTYpos(-2.20f);// wraps tank 1 at the top of the screen
		}
		else if (UPTGAME->playerPointer[i].getTYpos() < -2.20f)
		{
			UPTGAME->playerPointer[i].setTYpos(2.20f);// wraps tank 2 at the bottom of the screen
		}
	}
	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// this part contains some code that should be collapsed for now too...
#pragma region keep_this_bit_collapsed_too!

// this function is called when any events happen to our window
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{

	switch (message)
	{
		// if they exited the window...	
	case WM_DESTROY:
		// post a message "quit" message to the main windows loop
		PostQuitMessage(0);
		return 0;
		break;
	}

	// must do this as a default case (i.e. if no other event was handled)...
	return DefWindowProc(hwnd, message, wparam, lparam);

}

#pragma endregion